import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiliereComponent } from './filiere/filiere.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    FiliereComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    FiliereComponent
  ]
})
export class FiliereModule { }
