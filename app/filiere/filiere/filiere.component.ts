import { Component, OnInit } from '@angular/core';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'app-filiere',
  templateUrl: './filiere.component.html',
  styleUrls: ['./filiere.component.css']
})
export class FiliereComponent implements OnInit {

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
  }

}
