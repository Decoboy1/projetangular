import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Filiere } from '../interfaces/filiere.interface';

const api: string = 'http://localhost:8082/api/filiere';

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http:  HttpClient) { }

getFiliereAll(): Observable<Filiere[]>{ 
  return this.http.get<Filiere[]>(api);
}

getFiliereId(id: string): Observable<Filiere>{ 
return this.http.get<Filiere>(api + "/" + id);
}

// postFiliere(pers: Personne): Observable<HttpResponse<Personne>>{
//   const headers: HttpHeaders = new HttpHeaders()
//   .set('Content-Type', 'application/json');

//   console.log(JSON.stringify(pers));

//   return this.http.post<any>(api, JSON.stringify(pers), {headers, observe: 'response'})
// }

}