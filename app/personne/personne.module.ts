import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonneComponent } from './personne/personne.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FiliereComponent } from '../filiere/filiere/filiere.component';



@NgModule({
  declarations: [
    PersonneComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'page1', component: FiliereComponent}
    ])
    ],
  exports: [
    PersonneComponent
  ]
})
export class PersonneModule { }
