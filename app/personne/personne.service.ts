import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Personne } from '../interfaces/Personne.interface';
import { Observable } from 'rxjs';

const api: string = 'http://localhost:8082/api/personne';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {

  constructor(private http:  HttpClient) { }

  getPersonneAll(): Observable<Personne[]>{ 
      return this.http.get<Personne[]>(api);
  }

  getPersonneId(id: string): Observable<Personne>{ 
    return this.http.get<Personne>(api + "/" + id);
}

postPersonne(pers: Personne): Observable<HttpResponse<Personne>>{
  const headers: HttpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json');

  console.log(JSON.stringify(pers));

  return this.http.post<any>(api, JSON.stringify(pers), {headers, observe: 'response'})
}

}
