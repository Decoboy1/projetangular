import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs';
import { Personne } from 'src/app/interfaces/Personne.interface';
import { PersonneService } from '../personne.service';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Filiere } from 'src/app/interfaces/filiere.interface';
import { Personne2 } from 'src/app/interfaces/Personne2.interface';

@Component({
  selector: 'personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {

  personnes: Personne[] = [];
  personneById: Personne|null = null;
  selectedOption: string = "";
  id: string = "";
  personneForm: FormGroup = new FormGroup(
    {
      // id: new FormControl(),
      nom: new FormControl(""),
      prenom: new FormControl(""),
      type: new FormControl(""),
      filiere: new FormControl("")
    })

  constructor(private personneService: PersonneService) {}

  handleSelect(e: any){
      this.selectedOption = e.target.value;
  }

  handleFindAll(){
    const source$ = this.personneService.getPersonneAll(); 
  
    source$
    .pipe(
      switchMap( personnes => personnes )
    )
    .subscribe( personne => {
      let f: Filiere = {
        id: personne.filiere.id,
        libelle: personne.filiere.libelle,
        module: personne.filiere.module
      };
      let p: Personne =  {
       id: personne.id,
       nom: personne.nom,
       prenom: personne.prenom,
       type: personne.type,
       filiere: f
      }; 
      this.personnes.push(p)
    })
  }

  handleFindById(){

    //Pas réussi à récupérer la filiere 
    //et du coup à mettre à jour personneById... 

    const source$ = this.personneService
    .getPersonneId(this.id)
    .subscribe( 
      personne => {
        let f: Filiere = {
          id: personne.filiere.id,
          libelle: personne.filiere.libelle,
          module: personne.filiere.module
        };
        let p: Personne =  {
         id: personne.id,
         nom: personne.nom,
         prenom: personne.prenom,
         type: personne.type,
         filiere: f
        };

         this.personneById = p;       
      }
      );

       this.check();
  }

  check(){
    if(this.personneById!==null){
      console.log("filId : " + this.personneById.filiere)
    }
    else{
      console.log( "personneById est null")
    }

    console.log("this.personneById : " + this.personneById);
  }

  handleCreate(){
    let tmpPers: Personne = this.personneForm.value;
    this.personneService.postPersonne(tmpPers).subscribe( (res: HttpResponse<Personne>) => res );
  }

  ngOnInit(): void {
  }

}
